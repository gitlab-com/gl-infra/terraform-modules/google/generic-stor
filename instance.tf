resource "google_compute_address" "external" {
  count = var.use_external_ip ? var.node_count : 0

  project = local.subnetwork_project
  name = format(
    "%v-%02d-%v-%v-static-ip",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  address_type = "EXTERNAL"
  region       = var.region
}

resource "google_compute_address" "static-ip-address" {
  count = var.static_private_ip && var.ip_cidr_range != null ? var.node_count : 0

  project = local.subnetwork_project
  name = format(
    "%v-%02d-%v-%v-static-ip",
    var.name,
    count.index + 1 + 100,
    var.tier,
    var.environment,
  )
  address_type = "INTERNAL"

  address    = replace(var.ip_cidr_range, "/\\d+\\/\\d+$/", count.index + 1 + 100)
  subnetwork = local.subnetwork_self_link
  region     = var.region
}

resource "google_compute_address" "static-ip-address-multiple-disk-instance" {
  count = var.static_private_ip && var.ip_cidr_range != null && length(var.attached_disk) > 0 ? 1 : 0

  project = local.subnetwork_project
  name = format(
    "%v-%02d-%v-%v-static-ip",
    var.name,
    count.index + 1 + 100,
    var.tier,
    var.environment,
  )
  address_type = "INTERNAL"

  address    = replace(var.ip_cidr_range, "/\\d+\\/\\d+$/", count.index + 1 + 100)
  subnetwork = local.subnetwork_self_link
  region     = var.region
}

resource "google_compute_disk" "data_disk" {
  count = var.node_count

  project = var.project
  name = format(
    "%v-%02d-%v-%v-data",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  zone     = length(var.zones) != 0 ? element(var.zones, count.index) : element(data.google_compute_zones.available.names, count.index + 1)
  size     = var.data_disk_size
  type     = lookup(var.per_node_data_disk_type, count.index + 1, var.data_disk_type)
  snapshot = lookup(var.per_node_data_disk_snapshot, count.index + 1, var.data_disk_snapshot)

  labels = merge(var.labels, {
    environment  = var.environment
    pet_name     = var.name
    do_snapshots = "true"
  })

  lifecycle {
    ignore_changes = [snapshot]
  }

  timeouts {
    create = var.data_disk_create_timeout
  }

}

resource "google_compute_disk" "additional_data_disk" {
  for_each = {
    for i in var.attached_disk : i => {
      index = i
      name  = format("%v-%02d-%v-%v-data", var.name, i + 1, var.tier, var.environment)
    }
  }

  project = var.project
  name    = each.value.name
  zone    = length(var.zones) != 0 ? element(var.zones, 0) : element(data.google_compute_zones.available.names, 0)
  size    = lookup(var.per_node_data_disk_size, each.value.index, var.data_disk_size)
  type    = lookup(var.per_node_data_disk_type, each.value.index, var.data_disk_type)

  labels = merge(var.labels, {
    environment = var.environment
    pet_name    = var.name
  })

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [snapshot]
  }

  timeouts {
    create = var.data_disk_create_timeout
  }

}

resource "google_compute_attached_disk" "attached_data_disk" {
  for_each = toset(var.attached_disk)

  disk     = google_compute_disk.additional_data_disk[each.value].id
  instance = google_compute_instance.instance_with_multiple_attached_disk[0].id
}

resource "google_compute_disk_resource_policy_attachment" "data_disk_snapshot_policy_attachment" {
  count = var.hours_between_data_disk_snapshots > 0 ? var.node_count : 0

  project = var.project
  name    = google_compute_resource_policy.data_disk_snapshot_policy[count.index].name
  disk    = google_compute_disk.data_disk[count.index].name
  zone    = google_compute_disk.data_disk[count.index].zone

  lifecycle {
    # This lifecycle rule is necessary because you cannot modify policies when
    # they are in use
    replace_triggered_by = [
      google_compute_resource_policy.data_disk_snapshot_policy[count.index].id
    ]
  }
}

# Only take OS disk snapshots of the first instance in a given group.
resource "google_compute_disk_resource_policy_attachment" "instance_with_attached_disk_os_snap_policy_attachment" {
  count = var.enable_os_disk_snapshots && var.node_count > 0 && var.local_ssd_count == 0 ? 1 : 0

  project = var.project
  name    = google_compute_resource_policy.os_disk_snapshot_policy[0].name
  disk    = google_compute_instance.instance_with_attached_disk[0].name
  zone    = google_compute_instance.instance_with_attached_disk[0].zone

  lifecycle {
    replace_triggered_by = [
      google_compute_resource_policy.os_disk_snapshot_policy[0].id
    ]
  }
}

resource "google_compute_disk_resource_policy_attachment" "instance_with_local_ssds_os_snap_policy_attachment" {
  count = var.enable_os_disk_snapshots && var.node_count > 0 && var.local_ssd_count > 0 ? 1 : 0

  project = var.project
  name    = google_compute_resource_policy.os_disk_snapshot_policy[0].name
  disk    = google_compute_instance.instance_with_local_ssds[0].name
  zone    = google_compute_instance.instance_with_local_ssds[0].zone

  lifecycle {
    replace_triggered_by = [
      google_compute_resource_policy.os_disk_snapshot_policy[0].id
    ]
  }
}

resource "google_compute_disk_resource_policy_attachment" "instance_with_multi_disks_os_snap_policy_attachment" {
  count = var.enable_os_disk_snapshots && length(var.attached_disk) > 0 ? 1 : 0

  project = var.project
  name    = google_compute_resource_policy.os_disk_snapshot_policy[0].name
  disk    = google_compute_instance.instance_with_multiple_attached_disk[0].name
  zone    = google_compute_instance.instance_with_multiple_attached_disk[0].zone

  lifecycle {
    replace_triggered_by = [
      google_compute_resource_policy.os_disk_snapshot_policy[0].id
    ]
  }
}

resource "google_compute_disk" "os_disk_from_snapshot" {
  count = var.os_boot_disk_snapshot != "" ? var.node_count : 0

  zone     = length(var.zones) != 0 ? element(var.zones, count.index) : element(data.google_compute_zones.available.names, count.index + 1)
  labels   = var.labels
  snapshot = var.os_boot_disk_snapshot
  name     = format("%v-%02d-%v-%v", var.name, count.index + 1, var.tier, var.environment)
  type     = var.os_disk_type
  lifecycle {
    ignore_changes = [snapshot]
  }
}

resource "google_compute_disk" "log_disk" {
  count = var.node_count

  project = var.project
  name = format(
    "%v-%02d-%v-%v-log",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  zone = length(var.zones) != 0 ? element(var.zones, count.index) : element(data.google_compute_zones.available.names, count.index + 1)
  size = var.log_disk_size
  type = var.log_disk_type

  labels = merge(var.labels, {
    environment = var.environment
  })
}

resource "google_compute_disk" "instance_with_multiple_disk_log_disk" {
  count = length(var.attached_disk) > 0 ? 1 : 0

  project = var.project
  name = format(
    "%v-%v-%v-log",
    var.name,
    var.tier,
    var.environment,
  )
  zone = length(var.zones) != 0 ? element(var.zones, 0) : element(data.google_compute_zones.available.names, 0)
  size = var.log_disk_size
  type = var.log_disk_type

  labels = merge(var.labels, {
    environment = var.environment
  })
}

resource "google_compute_instance" "instance_with_attached_disk" {
  count = var.local_ssd_count == 0 ? var.node_count : 0

  project                   = var.project
  allow_stopping_for_update = var.allow_stopping_for_update
  deletion_protection       = lookup(var.per_node_deletion_protection, count.index + 1, var.deletion_protection)
  machine_type              = var.machine_type
  min_cpu_platform          = var.min_cpu_platform
  metadata = merge(
    local.instance_metadata,
    {
      "CHEF_NODE_NAME" = format(
        "%v-%02d-%v-%v.c.%v.internal",
        var.name,
        count.index + 1,
        var.tier,
        var.environment,
        var.project,
      )
    },
  )
  name = format(
    "%v-%02d-%v-%v",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  zone = length(var.zones) != 0 ? element(var.zones, count.index) : element(data.google_compute_zones.available.names, count.index + 1)

  service_account {
    // this should be the instance under which the instance should be running, rather than the one creating it...
    email  = var.service_account_email
    scopes = local.service_account_scopes
  }

  scheduling {
    preemptible = var.preemptible
  }

  boot_disk {
    auto_delete = true
    source      = var.os_boot_disk_snapshot == "" ? null : google_compute_disk.os_disk_from_snapshot[count.index].self_link
    dynamic "initialize_params" {
      for_each = var.os_boot_disk_snapshot == "" ? [0] : []

      content {
        image  = var.os_boot_image
        size   = var.os_disk_size
        type   = var.os_disk_type
        labels = var.labels
      }
    }
  }

  attached_disk {
    source      = google_compute_disk.data_disk[count.index].self_link
    device_name = var.data_disk_name
  }

  attached_disk {
    source      = google_compute_disk.log_disk[count.index].self_link
    device_name = "log"
  }

  network_interface {
    subnetwork = local.subnetwork_self_link
    network_ip = var.static_private_ip ? google_compute_address.static-ip-address[count.index].address : null

    dynamic "access_config" {
      for_each = var.assign_public_ip ? [0] : []
      content {
        nat_ip = var.use_external_ip ? element(local.external_ips, count.index) : ""
      }
    }
  }

  labels = merge(var.labels, {
    environment = var.environment
    pet_name    = var.name
  })

  tags = [
    var.name,
    var.environment,
  ]

  lifecycle {
    ignore_changes = [min_cpu_platform, boot_disk, machine_type]
  }
}

resource "google_compute_instance" "instance_with_local_ssds" {
  count = var.local_ssd_count > 0 ? var.node_count : 0

  project                   = var.project
  allow_stopping_for_update = var.allow_stopping_for_update
  deletion_protection       = var.deletion_protection
  machine_type              = var.machine_type
  min_cpu_platform          = var.min_cpu_platform
  metadata = merge(
    local.instance_metadata,
    {
      "CHEF_NODE_NAME" = format(
        "%v-%02d-%v-%v.c.%v.internal",
        var.name,
        count.index + 1,
        var.tier,
        var.environment,
        var.project,
      )
    },
  )

  name = format(
    "%v-%02d-%v-%v",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  zone = length(var.zones) != 0 ? element(var.zones, count.index) : element(data.google_compute_zones.available.names, count.index + 1)

  service_account {
    // this should be the instance under which the instance should be running, rather than the one creating it...
    email  = var.service_account_email
    scopes = local.service_account_scopes
  }

  scheduling {
    preemptible = var.preemptible
  }

  boot_disk {
    auto_delete = true
    source      = var.os_boot_disk_snapshot == "" ? null : google_compute_disk.os_disk_from_snapshot[count.index].self_link
    dynamic "initialize_params" {
      for_each = var.os_boot_disk_snapshot == "" ? [0] : []

      content {
        image  = var.os_boot_image
        size   = var.os_disk_size
        type   = var.os_disk_type
        labels = var.labels
      }
    }
  }

  attached_disk {
    source      = google_compute_disk.data_disk[count.index].self_link
    device_name = var.data_disk_name
  }

  attached_disk {
    source      = google_compute_disk.log_disk[count.index].self_link
    device_name = "log"
  }

  # In terraform < 0.12, there is no way to create a configurable number of
  # inline blocks. The only way to provision GCP local SSDs for instances is
  # using these inline blocks.
  # TODO after terraform 0.12 upgrade, de-duplicate the instances and loop over
  # local SSDs using a configured count.
  scratch_disk {
    interface = "NVME"
  }

  scratch_disk {
    interface = "NVME"
  }

  network_interface {
    subnetwork = local.subnetwork_self_link
    network_ip = var.static_private_ip ? google_compute_address.static-ip-address[count.index].address : null

    dynamic "access_config" {
      for_each = var.assign_public_ip ? [0] : []
      content {
        nat_ip = var.use_external_ip ? element(local.external_ips, count.index) : ""
      }
    }
  }

  labels = merge(var.labels, {
    environment = var.environment
    pet_name    = var.name
  })

  tags = [
    var.name,
    var.environment,
  ]

  lifecycle {
    ignore_changes = [min_cpu_platform, boot_disk, machine_type]
  }
}

resource "google_compute_instance" "instance_with_multiple_attached_disk" {
  count = length(var.attached_disk) > 0 ? 1 : 0

  project                   = var.project
  allow_stopping_for_update = var.allow_stopping_for_update
  deletion_protection       = var.deletion_protection
  machine_type              = var.machine_type
  min_cpu_platform          = var.min_cpu_platform
  metadata = merge(
    local.instance_metadata,
    {
      "CHEF_NODE_NAME" = format(
        "%v-%02d-%v-%v.c.%v.internal",
        var.name,
        1,
        var.tier,
        var.environment,
        var.project,
      )
    },
  )

  name = format(
    "%v-%02d-%v-%v",
    var.name,
    1,
    var.tier,
    var.environment,
  )
  zone = length(var.zones) != 0 ? element(var.zones, 0) : element(data.google_compute_zones.available.names, 0)

  service_account {
    // this should be the instance under which the instance should be running, rather than the one creating it...
    email  = var.service_account_email
    scopes = local.service_account_scopes
  }

  scheduling {
    preemptible = var.preemptible
  }

  boot_disk {
    auto_delete = true
    source      = var.os_boot_disk_snapshot == "" ? null : google_compute_disk.os_disk_from_snapshot[count.index].self_link
    dynamic "initialize_params" {
      for_each = var.os_boot_disk_snapshot == "" ? [0] : []

      content {
        image  = var.os_boot_image
        size   = var.os_disk_size
        type   = var.os_disk_type
        labels = var.labels
      }
    }
  }

  attached_disk {
    source      = google_compute_disk.instance_with_multiple_disk_log_disk[count.index].self_link
    device_name = "log"
  }

  network_interface {
    subnetwork = local.subnetwork_self_link
    network_ip = var.static_private_ip ? google_compute_address.static-ip-address-multiple-disk-instance[0].address : null
    dynamic "access_config" {
      for_each = var.assign_public_ip ? [0] : []
      content {
        nat_ip = var.use_external_ip ? element(local.external_ips, 0) : ""
      }
    }
  }

  labels = merge(var.labels, {
    environment = var.environment
    pet_name    = var.name
  })

  tags = [
    var.name,
    var.environment,
  ]

  lifecycle {
    ignore_changes = [min_cpu_platform, boot_disk, machine_type, attached_disk]
  }
}
