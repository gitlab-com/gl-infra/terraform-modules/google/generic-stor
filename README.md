# GitLab.com Generic Storage Terraform Module

## What is this?

This module provisions generic storage GCE instances.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.0.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_bootstrap"></a> [bootstrap](#module\_bootstrap) | ops.gitlab.net/gitlab-com/bootstrap/google | 5.5.8 |

## Resources

| Name | Type |
|------|------|
| [google_compute_address.external](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_address.multizone-static-ip-address](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_address.multizone_external](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_address.static-ip-address](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_address.static-ip-address-multiple-disk-instance](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_attached_disk.attached_data_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_attached_disk) | resource |
| [google_compute_disk.additional_data_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.data_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.instance_with_multiple_disk_log_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.log_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.multizone_data_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.multizone_log_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.multizone_os_disk_from_snapshot](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk.os_disk_from_snapshot](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk) | resource |
| [google_compute_disk_resource_policy_attachment.data_disk_snapshot_policy_attachment](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk_resource_policy_attachment) | resource |
| [google_compute_disk_resource_policy_attachment.instance_with_attached_disk_os_snap_policy_attachment](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk_resource_policy_attachment) | resource |
| [google_compute_disk_resource_policy_attachment.instance_with_local_ssds_os_snap_policy_attachment](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk_resource_policy_attachment) | resource |
| [google_compute_disk_resource_policy_attachment.instance_with_multi_disks_os_snap_policy_attachment](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk_resource_policy_attachment) | resource |
| [google_compute_disk_resource_policy_attachment.multizone_data_disk_snapshot_policy_attachment](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk_resource_policy_attachment) | resource |
| [google_compute_disk_resource_policy_attachment.mz_instance_with_attached_disk_os_snap_policy_attachment](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk_resource_policy_attachment) | resource |
| [google_compute_disk_resource_policy_attachment.mz_instance_with_local_ssds_os_snap_policy_attachment](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_disk_resource_policy_attachment) | resource |
| [google_compute_firewall.deny_all_egress](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.monitoring_whitelist](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.public](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.to_network](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.to_world](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_instance.instance_with_attached_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance) | resource |
| [google_compute_instance.instance_with_local_ssds](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance) | resource |
| [google_compute_instance.instance_with_multiple_attached_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance) | resource |
| [google_compute_instance.multizone_instance_with_attached_disk](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance) | resource |
| [google_compute_instance.multizone_instance_with_local_ssds](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance) | resource |
| [google_compute_resource_policy.data_disk_snapshot_policy](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_resource_policy) | resource |
| [google_compute_resource_policy.multizone_data_disk_snapshot_policy](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_resource_policy) | resource |
| [google_compute_resource_policy.os_disk_snapshot_policy](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_resource_policy) | resource |
| [google_compute_subnetwork.subnetwork](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_subnetwork) | resource |
| [google_compute_subnetwork.subnetwork](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_subnetwork) | data source |
| [google_compute_zones.available](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_zones) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allow_stopping_for_update"></a> [allow\_stopping\_for\_update](#input\_allow\_stopping\_for\_update) | Whether Terraform is allowed to stop the instance to update its properties | `bool` | `false` | no |
| <a name="input_assign_public_ip"></a> [assign\_public\_ip](#input\_assign\_public\_ip) | Instances without public IPs cannot access the public internet without NAT. Ensure that a Cloud NAT instance covers the subnetwork/region for this instance. | `bool` | `true` | no |
| <a name="input_attached_disk"></a> [attached\_disk](#input\_attached\_disk) | A unique identifier of the attached disks to create | `list(string)` | `[]` | no |
| <a name="input_block_project_ssh_keys"></a> [block\_project\_ssh\_keys](#input\_block\_project\_ssh\_keys) | Whether to block project level SSH keys | `bool` | `true` | no |
| <a name="input_bootstrap_data_disk"></a> [bootstrap\_data\_disk](#input\_bootstrap\_data\_disk) | Bootstrap script should detect, format, and mount data disk | `string` | `"true"` | no |
| <a name="input_bootstrap_script"></a> [bootstrap\_script](#input\_bootstrap\_script) | user-provided bootstrap script to override the bootstrap module | `string` | `null` | no |
| <a name="input_chef_init_run_list"></a> [chef\_init\_run\_list](#input\_chef\_init\_run\_list) | run\_list for the node in chef that are ran on the first boot only | `string` | `""` | no |
| <a name="input_chef_provision"></a> [chef\_provision](#input\_chef\_provision) | Configuration details for chef server | `map(string)` | n/a | yes |
| <a name="input_chef_run_list"></a> [chef\_run\_list](#input\_chef\_run\_list) | run\_list for the node in chef | `string` | n/a | yes |
| <a name="input_data_disk_create_timeout"></a> [data\_disk\_create\_timeout](#input\_data\_disk\_create\_timeout) | Timeout applied for creating the data disk. This operation can take a long time if restoring from snapshot. | `string` | `"30m"` | no |
| <a name="input_data_disk_name"></a> [data\_disk\_name](#input\_data\_disk\_name) | If set, the block device file will be symlinked at /dev/disk/by-id/google-$NAME | `string` | `""` | no |
| <a name="input_data_disk_size"></a> [data\_disk\_size](#input\_data\_disk\_size) | The size of the data disk | `number` | `20` | no |
| <a name="input_data_disk_snapshot"></a> [data\_disk\_snapshot](#input\_data\_disk\_snapshot) | Restore data disk from a snapshot | `string` | `""` | no |
| <a name="input_data_disk_snapshot_max_retention_days"></a> [data\_disk\_snapshot\_max\_retention\_days](#input\_data\_disk\_snapshot\_max\_retention\_days) | Number of days to retain snapshots, defaults to 2 weeks | `number` | `14` | no |
| <a name="input_data_disk_type"></a> [data\_disk\_type](#input\_data\_disk\_type) | The type of the data disk | `string` | `"pd-standard"` | no |
| <a name="input_deletion_protection"></a> [deletion\_protection](#input\_deletion\_protection) | n/a | `bool` | `false` | no |
| <a name="input_dns_zone_name"></a> [dns\_zone\_name](#input\_dns\_zone\_name) | The GCP name of the DNS zone to use for this environment | `string` | n/a | yes |
| <a name="input_egress_ports"></a> [egress\_ports](#input\_egress\_ports) | The list of ports that should be opened for egress traffic | `list(string)` | `[]` | no |
| <a name="input_enable_os_disk_snapshots"></a> [enable\_os\_disk\_snapshots](#input\_enable\_os\_disk\_snapshots) | Enable creation of OS disk snapshot resource and attachment policies | `bool` | `false` | no |
| <a name="input_enable_os_snapshot_guest_scripts"></a> [enable\_os\_snapshot\_guest\_scripts](#input\_enable\_os\_snapshot\_guest\_scripts) | Enable application-consistent snapshots so that pre and post snap scripts are executed by default. | `bool` | `true` | no |
| <a name="input_enable_oslogin"></a> [enable\_oslogin](#input\_enable\_oslogin) | Whether to enable OS Login GCP feature | `bool` | `false` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | The environment name | `string` | n/a | yes |
| <a name="input_format_data_disk"></a> [format\_data\_disk](#input\_format\_data\_disk) | Force formatting of the persistent disk. | `string` | `"false"` | no |
| <a name="input_hours_between_data_disk_snapshots"></a> [hours\_between\_data\_disk\_snapshots](#input\_hours\_between\_data\_disk\_snapshots) | Setting this will enable hourly data disk snapshots with staggered start times, 0 to disable | `number` | `0` | no |
| <a name="input_hours_between_os_disk_snapshots"></a> [hours\_between\_os\_disk\_snapshots](#input\_hours\_between\_os\_disk\_snapshots) | Setting this will enable hourly os disk snapshots, 0 to disable | `number` | `0` | no |
| <a name="input_initial_boot_force_reboot"></a> [initial\_boot\_force\_reboot](#input\_initial\_boot\_force\_reboot) | Force an additional reboot after the initial boot, this ensures the log disk is cleanly mounted after a rebuild. | `string` | `"false"` | no |
| <a name="input_ip_cidr_range"></a> [ip\_cidr\_range](#input\_ip\_cidr\_range) | Subnetwork IP range for creating a new subnet, if not set you must set subnetwork\_self\_link | `string` | `null` | no |
| <a name="input_kernel_version"></a> [kernel\_version](#input\_kernel\_version) | n/a | `string` | `""` | no |
| <a name="input_labels"></a> [labels](#input\_labels) | Labels to add to each of the managed resources. | `map(string)` | `{}` | no |
| <a name="input_local_ssd_count"></a> [local\_ssd\_count](#input\_local\_ssd\_count) | If non-zero, attaches exactly 2 local SSDs to the instance. This can be made better with Terraform 0.12. | `number` | `0` | no |
| <a name="input_log_disk_size"></a> [log\_disk\_size](#input\_log\_disk\_size) | The size of the log disk | `number` | `50` | no |
| <a name="input_log_disk_type"></a> [log\_disk\_type](#input\_log\_disk\_type) | The type of the log disk | `string` | `"pd-standard"` | no |
| <a name="input_machine_type"></a> [machine\_type](#input\_machine\_type) | The machine size | `string` | n/a | yes |
| <a name="input_min_cpu_platform"></a> [min\_cpu\_platform](#input\_min\_cpu\_platform) | Optional minimum CPU platform, some machine types support specifying a specific architecture version, e.g. AMD Milan (https://cloud.google.com/compute/docs/instances/specify-min-cpu-platform#availablezones) | `string` | `null` | no |
| <a name="input_monitoring_whitelist"></a> [monitoring\_whitelist](#input\_monitoring\_whitelist) | n/a | <pre>object({<br/>    subnets = list(string)<br/>    ports   = list(string)<br/>  })</pre> | <pre>{<br/>  "ports": [],<br/>  "subnets": []<br/>}</pre> | no |
| <a name="input_multizone_node_count"></a> [multizone\_node\_count](#input\_multizone\_node\_count) | The nodes count | `number` | `0` | no |
| <a name="input_name"></a> [name](#input\_name) | The pet name | `string` | n/a | yes |
| <a name="input_node_count"></a> [node\_count](#input\_node\_count) | The nodes count | `number` | n/a | yes |
| <a name="input_os_boot_disk_snapshot"></a> [os\_boot\_disk\_snapshot](#input\_os\_boot\_disk\_snapshot) | If set to the self\_link of a snapshot, this will be used to provision the boot\_disk for new instances. | `string` | `""` | no |
| <a name="input_os_boot_image"></a> [os\_boot\_image](#input\_os\_boot\_image) | The OS image to boot | `string` | `"ubuntu-os-cloud/ubuntu-2004-lts"` | no |
| <a name="input_os_disk_size"></a> [os\_disk\_size](#input\_os\_disk\_size) | The OS disk size in GiB | `number` | `20` | no |
| <a name="input_os_disk_snapshot_max_retention_days"></a> [os\_disk\_snapshot\_max\_retention\_days](#input\_os\_disk\_snapshot\_max\_retention\_days) | Number of days to retain snapshots, defaults to 3. | `number` | `3` | no |
| <a name="input_os_disk_type"></a> [os\_disk\_type](#input\_os\_disk\_type) | The OS disk type | `string` | `"pd-standard"` | no |
| <a name="input_per_node_data_disk_size"></a> [per\_node\_data\_disk\_size](#input\_per\_node\_data\_disk\_size) | Override data disk size (var.data\_disk\_size) for certain nodes. Map key should be the node number as an integer, starting at 1 | `map(string)` | `{}` | no |
| <a name="input_per_node_data_disk_snapshot"></a> [per\_node\_data\_disk\_snapshot](#input\_per\_node\_data\_disk\_snapshot) | Override data disk snapshot for certain nodes. Map key should be the node number as an integer, starting at 1 | `map(string)` | `{}` | no |
| <a name="input_per_node_data_disk_snapshot_max_retention_days"></a> [per\_node\_data\_disk\_snapshot\_max\_retention\_days](#input\_per\_node\_data\_disk\_snapshot\_max\_retention\_days) | Override the number of days snapshots are retained for the data disk on select nodes. Map key should be the node number as an integer, starting at 1 | `map(number)` | `{}` | no |
| <a name="input_per_node_data_disk_type"></a> [per\_node\_data\_disk\_type](#input\_per\_node\_data\_disk\_type) | Override data disk type (var.data\_disk\_type) for certain nodes. Map key should be the node number as an integer, starting at 1 | `map(string)` | `{}` | no |
| <a name="input_per_node_deletion_protection"></a> [per\_node\_deletion\_protection](#input\_per\_node\_deletion\_protection) | Overrides deletion protection. Map key should be the node number as an integer, starting at 1 | `map(string)` | `{}` | no |
| <a name="input_per_node_hours_between_data_disk_snapshots"></a> [per\_node\_hours\_between\_data\_disk\_snapshots](#input\_per\_node\_hours\_between\_data\_disk\_snapshots) | Override the number of hours between data disk snapshots (var.hours\_between\_data\_disk\_snapshots) for select nodes. Map key should be the node number as an integer, starting at 1 | `map(number)` | `{}` | no |
| <a name="input_persistent_disk_path"></a> [persistent\_disk\_path](#input\_persistent\_disk\_path) | default location for disk mount | `string` | `"/var/opt/gitlab"` | no |
| <a name="input_preemptible"></a> [preemptible](#input\_preemptible) | Use preemptible instances for this pet | `bool` | `false` | no |
| <a name="input_project"></a> [project](#input\_project) | The project name | `string` | n/a | yes |
| <a name="input_public_ports"></a> [public\_ports](#input\_public\_ports) | The list of ports that should be publicly reachable | `list(string)` | `[]` | no |
| <a name="input_region"></a> [region](#input\_region) | The target region | `string` | n/a | yes |
| <a name="input_service_account_email"></a> [service\_account\_email](#input\_service\_account\_email) | Service account emails under which the instance is running | `string` | n/a | yes |
| <a name="input_static_private_ip"></a> [static\_private\_ip](#input\_static\_private\_ip) | Assign a static private IP, with deterministic address based on node count. Should be false for new module instances, this only exists for backwards compatibility. You must also set ip\_cidr\_range when set to true. | `bool` | `true` | no |
| <a name="input_subnetwork_self_link"></a> [subnetwork\_self\_link](#input\_subnetwork\_self\_link) | Self link of subnetwork to allocate IPs/instances in. If not specified, you must set ip\_cidr\_range | `string` | `null` | no |
| <a name="input_teardown_script"></a> [teardown\_script](#input\_teardown\_script) | user-provided teardown script to override the bootstrap module | `string` | `null` | no |
| <a name="input_tier"></a> [tier](#input\_tier) | The tier for this service | `string` | n/a | yes |
| <a name="input_use_external_ip"></a> [use\_external\_ip](#input\_use\_external\_ip) | n/a | `bool` | `false` | no |
| <a name="input_vpc"></a> [vpc](#input\_vpc) | The target network | `string` | n/a | yes |
| <a name="input_zones"></a> [zones](#input\_zones) | user-provided list of zones to that when provided will override using all available zones | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_google_compute_address_static_ips"></a> [google\_compute\_address\_static\_ips](#output\_google\_compute\_address\_static\_ips) | n/a |
| <a name="output_google_compute_subnetwork_self_link"></a> [google\_compute\_subnetwork\_self\_link](#output\_google\_compute\_subnetwork\_self\_link) | n/a |
| <a name="output_instances_self_link"></a> [instances\_self\_link](#output\_instances\_self\_link) | n/a |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
