data "google_compute_subnetwork" "subnetwork" {
  # We use this data resource to get the project that corresponds
  # to the subnetwork_self_link passed into the module.
  count = var.ip_cidr_range == null ? 1 : 0

  self_link = var.subnetwork_self_link

  lifecycle {
    precondition {
      # fails if this condition evaluates to false (if var.subnetwork_self_link is null)
      condition     = var.subnetwork_self_link != null
      error_message = "you must set either ip_cidr_range and subnetwork_self_link in this module!"
    }
  }
}

locals {
  subnetwork_project   = coalesce(one(data.google_compute_subnetwork.subnetwork[*].project), var.project)
  subnetwork_self_link = coalesce(var.subnetwork_self_link, one(google_compute_subnetwork.subnetwork[*].self_link))
}

resource "google_compute_subnetwork" "subnetwork" {
  # Create the subnetwork if an `ip_cidr_range` is provided,
  # in this case var.subnetwork_self_link should be `null`.
  count = var.ip_cidr_range == null ? 0 : 1

  project                  = var.project
  name                     = format("%v-%v", var.name, var.environment)
  network                  = var.vpc
  region                   = var.region
  ip_cidr_range            = var.ip_cidr_range
  private_ip_google_access = true

  lifecycle {
    precondition {
      # fails if this condition evaluates to false (if var.subnetwork_self_link has a value)
      condition     = var.subnetwork_self_link == null
      error_message = "you cannot set both ip_cidr_range and subnetwork_self_link in this module!"
    }
  }
}
