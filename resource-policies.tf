resource "google_compute_resource_policy" "multizone_data_disk_snapshot_policy" {
  count = var.hours_between_data_disk_snapshots > 0 ? var.multizone_node_count : 0

  project = var.project
  name = format(
    "%v-%02d-multizone-snapshot-policy",
    var.name,
    count.index + var.node_count + 1,
  )

  snapshot_schedule_policy {
    schedule {
      hourly_schedule {
        hours_in_cycle = lookup(var.per_node_hours_between_data_disk_snapshots, count.index + var.node_count + 1, var.hours_between_data_disk_snapshots)
        # Time within the window to start the operations.
        # It must be in an hourly format "HH:MM", where HH : [00-23] and MM : [00] GMT. eg: 21:00
        # If a one hour cycle is used the start_time is not needed so we set it 00:00, so it isn't set in the future.
        start_time = lookup(var.per_node_hours_between_data_disk_snapshots, count.index + var.node_count + 1, var.hours_between_data_disk_snapshots) == 1 ? "00:00" : format("%02d:00", count.index % 24)
      }
    }

    retention_policy {
      max_retention_days    = lookup(var.per_node_data_disk_snapshot_max_retention_days, count.index + var.node_count + 1, var.data_disk_snapshot_max_retention_days)
      on_source_disk_delete = "APPLY_RETENTION_POLICY"
    }

    dynamic "snapshot_properties" {
      for_each = length(var.labels) > 0 ? [true] : []
      content {
        labels = var.labels
      }
    }
  }
}

resource "google_compute_resource_policy" "data_disk_snapshot_policy" {
  count = var.hours_between_data_disk_snapshots > 0 ? var.node_count : 0

  project = var.project
  name = format(
    "%v-%02d-snapshot-policy",
    var.name,
    count.index + 1,
  )

  snapshot_schedule_policy {
    schedule {
      hourly_schedule {
        hours_in_cycle = lookup(var.per_node_hours_between_data_disk_snapshots, count.index + 1, var.hours_between_data_disk_snapshots)
        # Time within the window to start the operations.
        # It must be in an hourly format "HH:MM", where HH : [00-23] and MM : [00] GMT. eg: 21:00
        # If a one hour cycle is used the start_time is not needed so we set it to 00:00, so it isn't set in the future.
        start_time = lookup(var.per_node_hours_between_data_disk_snapshots, count.index + 1, var.hours_between_data_disk_snapshots) == 1 ? "00:00" : format("%02d:00", count.index % 24)
      }
    }

    retention_policy {
      max_retention_days    = lookup(var.per_node_data_disk_snapshot_max_retention_days, count.index + 1, var.data_disk_snapshot_max_retention_days)
      on_source_disk_delete = "APPLY_RETENTION_POLICY"
    }

    dynamic "snapshot_properties" {
      for_each = length(var.labels) > 0 ? [true] : []
      content {
        labels = var.labels
      }
    }
  }
}

resource "google_compute_resource_policy" "os_disk_snapshot_policy" {
  count = var.enable_os_disk_snapshots ? 1 : 0

  project = var.project

  name = format(
    "%v-os-snapshot-policy",
    var.name
  )

  snapshot_schedule_policy {
    schedule {
      hourly_schedule {
        hours_in_cycle = var.hours_between_os_disk_snapshots
        start_time     = "00:00"
      }
    }

    snapshot_properties {
      guest_flush = var.enable_os_snapshot_guest_scripts
      labels      = var.labels
    }

    retention_policy {
      max_retention_days    = var.os_disk_snapshot_max_retention_days
      on_source_disk_delete = "APPLY_RETENTION_POLICY"
    }
  }
}
