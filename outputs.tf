output "instances_self_link" {
  value = google_compute_instance.instance_with_attached_disk[*].self_link
}

output "google_compute_address_static_ips" {
  value = element(
    concat(google_compute_address.static-ip-address[*].address, [""]),
    0,
  )
}

output "google_compute_subnetwork_self_link" {
  value = one(google_compute_subnetwork.subnetwork[*].self_link)
}
