variable "monitoring_whitelist" {
  type = object({
    subnets = list(string)
    ports   = list(string)
  })

  default = {
    "subnets" = []
    "ports"   = []
  }
}

variable "data_disk_snapshot" {
  type        = string
  description = "Restore data disk from a snapshot"
  # An empty string will not use a snapshot for the data disk
  default = ""
}

variable "hours_between_data_disk_snapshots" {
  type        = number
  description = "Setting this will enable hourly data disk snapshots with staggered start times, 0 to disable"
  default     = 0
}

variable "data_disk_snapshot_max_retention_days" {
  type        = number
  description = "Number of days to retain snapshots, defaults to 2 weeks"
  default     = 14
}

variable "enable_os_disk_snapshots" {
  type        = bool
  description = "Enable creation of OS disk snapshot resource and attachment policies"
  default     = false
}

variable "os_disk_snapshot_max_retention_days" {
  type        = number
  description = "Number of days to retain snapshots, defaults to 3."
  default     = 3
}

variable "os_boot_disk_snapshot" {
  type        = string
  description = "If set to the self_link of a snapshot, this will be used to provision the boot_disk for new instances."
  default     = ""
}

# Enable application-consistent snapshots so that pre and post snap scripts are executed by default.
variable "enable_os_snapshot_guest_scripts" {
  type    = bool
  default = true
}

variable "hours_between_os_disk_snapshots" {
  type        = number
  description = "Setting this will enable hourly os disk snapshots, 0 to disable"
  default     = 0
}

variable "use_external_ip" {
  type    = bool
  default = false
}

variable "kernel_version" {
  type    = string
  default = ""
}

variable "allow_stopping_for_update" {
  type        = bool
  description = "Whether Terraform is allowed to stop the instance to update its properties"
  default     = false
}

variable "block_project_ssh_keys" {
  type        = bool
  description = "Whether to block project level SSH keys"
  default     = true
}

variable "persistent_disk_path" {
  type        = string
  description = "default location for disk mount"
  default     = "/var/opt/gitlab"
}

variable "chef_init_run_list" {
  type        = string
  default     = ""
  description = "run_list for the node in chef that are ran on the first boot only"
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"
}

variable "chef_run_list" {
  type        = string
  description = "run_list for the node in chef"
}

variable "data_disk_size" {
  type        = number
  description = "The size of the data disk"
  default     = 20
}

variable "per_node_data_disk_size" {
  type        = map(string)
  description = "Override data disk size (var.data_disk_size) for certain nodes. Map key should be the node number as an integer, starting at 1"
  default     = {}
}

variable "data_disk_type" {
  type        = string
  description = "The type of the data disk"
  default     = "pd-standard"
}

variable "per_node_data_disk_type" {
  type        = map(string)
  description = "Override data disk type (var.data_disk_type) for certain nodes. Map key should be the node number as an integer, starting at 1"
  default     = {}
}

variable "per_node_deletion_protection" {
  type        = map(string)
  description = "Overrides deletion protection. Map key should be the node number as an integer, starting at 1"
  default     = {}
}

variable "per_node_data_disk_snapshot" {
  type        = map(string)
  description = "Override data disk snapshot for certain nodes. Map key should be the node number as an integer, starting at 1"
  default     = {}
}

variable "per_node_hours_between_data_disk_snapshots" {
  type        = map(number)
  description = "Override the number of hours between data disk snapshots (var.hours_between_data_disk_snapshots) for select nodes. Map key should be the node number as an integer, starting at 1"
  default     = {}
}

variable "per_node_data_disk_snapshot_max_retention_days" {
  type        = map(number)
  description = "Override the number of days snapshots are retained for the data disk on select nodes. Map key should be the node number as an integer, starting at 1"
  default     = {}
}

variable "dns_zone_name" {
  type        = string
  description = "The GCP name of the DNS zone to use for this environment"
}

variable "egress_ports" {
  type        = list(string)
  description = "The list of ports that should be opened for egress traffic"
  default     = []
}

variable "enable_oslogin" {
  type        = bool
  description = "Whether to enable OS Login GCP feature"
  # Note: setting this to TRUE breaks chef!
  # https://gitlab.com/gitlab-com/gitlab-com-infrastructure/merge_requests/297#note_66690562
  default = false
}

variable "environment" {
  type        = string
  description = "The environment name"
}

variable "log_disk_size" {
  type        = number
  description = "The size of the log disk"
  default     = 50
}

variable "log_disk_type" {
  type        = string
  description = "The type of the log disk"
  default     = "pd-standard"
}

variable "bootstrap_data_disk" {
  type        = string
  description = "Bootstrap script should detect, format, and mount data disk"
  default     = "true"
}

variable "format_data_disk" {
  type        = string
  description = "Force formatting of the persistent disk."
  default     = "false"
}

variable "initial_boot_force_reboot" {
  type        = string
  description = "Force an additional reboot after the initial boot, this ensures the log disk is cleanly mounted after a rebuild."
  default     = "false"
}

variable "ip_cidr_range" {
  type        = string
  description = "Subnetwork IP range for creating a new subnet, if not set you must set subnetwork_self_link"
  default     = null
}

variable "machine_type" {
  type        = string
  description = "The machine size"
}

variable "min_cpu_platform" {
  type        = string
  description = "Optional minimum CPU platform, some machine types support specifying a specific architecture version, e.g. AMD Milan (https://cloud.google.com/compute/docs/instances/specify-min-cpu-platform#availablezones)"
  default     = null
}

variable "multizone_node_count" {
  type        = number
  description = "The nodes count"
  default     = 0
}

variable "name" {
  type        = string
  description = "The pet name"
}

variable "node_count" {
  type        = number
  description = "The nodes count"
}

variable "os_boot_image" {
  type        = string
  description = "The OS image to boot"
  default     = "ubuntu-os-cloud/ubuntu-2004-lts"
}

variable "os_disk_size" {
  type        = number
  description = "The OS disk size in GiB"
  default     = 20
}

variable "os_disk_type" {
  type        = string
  description = "The OS disk type"
  default     = "pd-standard"
}

variable "preemptible" {
  type        = bool
  description = "Use preemptible instances for this pet"
  default     = false
}

variable "project" {
  type        = string
  description = "The project name"
}

variable "public_ports" {
  type        = list(string)
  description = "The list of ports that should be publicly reachable"
  default     = []
}

variable "region" {
  type        = string
  description = "The target region"
}

variable "service_account_email" {
  type        = string
  description = "Service account emails under which the instance is running"
}

variable "bootstrap_script" {
  type        = string
  description = "user-provided bootstrap script to override the bootstrap module"
  default     = null
}

variable "teardown_script" {
  type        = string
  description = "user-provided teardown script to override the bootstrap module"
  default     = null
}

variable "tier" {
  type        = string
  description = "The tier for this service"
}

variable "vpc" {
  type        = string
  description = "The target network"
}

variable "zones" {
  type        = list(string)
  description = "user-provided list of zones to that when provided will override using all available zones"
  default     = []
}

variable "deletion_protection" {
  type    = bool
  default = false
}

# TODO In Terraform pre-0.12, you can't create a configurable number of inline
# blocks. After the upgrade to 0.12, change the description
variable "local_ssd_count" {
  type        = number
  description = "If non-zero, attaches exactly 2 local SSDs to the instance. This can be made better with Terraform 0.12."
  default     = 0
}

variable "data_disk_name" {
  type        = string
  default     = ""
  description = "If set, the block device file will be symlinked at /dev/disk/by-id/google-$NAME"
}

variable "data_disk_create_timeout" {
  type        = string
  description = "Timeout applied for creating the data disk. This operation can take a long time if restoring from snapshot."
  default     = "30m"
}

# Instances without public IPs cannot access the public internet without NAT.
# Ensure that a Cloud NAT instance covers the subnetwork/region for this
# instance.
variable "assign_public_ip" {
  type    = bool
  default = true
}

variable "subnetwork_self_link" {
  type        = string
  description = "Self link of subnetwork to allocate IPs/instances in. If not specified, you must set ip_cidr_range"
  default     = null
}

variable "static_private_ip" {
  type        = bool
  description = "Assign a static private IP, with deterministic address based on node count. Should be false for new module instances, this only exists for backwards compatibility. You must also set ip_cidr_range when set to true."
  default     = true
}

# Resource labels
variable "labels" {
  type        = map(string)
  description = "Labels to add to each of the managed resources."
  default     = {}
}

variable "attached_disk" {
  type        = list(string)
  description = "A unique identifier of the attached disks to create"
  default     = []
}
