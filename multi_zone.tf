resource "google_compute_address" "multizone_external" {
  count = var.use_external_ip ? var.multizone_node_count : 0

  project      = local.subnetwork_project
  name         = local.multi_zone_node_names[count.index].static_ip_external_name
  address_type = "EXTERNAL"
}

resource "google_compute_address" "multizone-static-ip-address" {
  count = var.static_private_ip && var.ip_cidr_range != null ? var.multizone_node_count : 0

  project      = local.subnetwork_project
  name         = local.multi_zone_node_names[count.index].static_ip_internal_name
  address_type = "INTERNAL"

  address = replace(
    var.ip_cidr_range,
    "/\\d+\\/\\d+$/",
    count.index + var.node_count + 1 + 100,
  )
  subnetwork = coalesce(var.subnetwork_self_link, one(google_compute_subnetwork.subnetwork[*].self_link))
}

resource "google_compute_disk" "multizone_data_disk" {
  count = var.multizone_node_count

  project = var.project
  name    = local.multi_zone_node_names[count.index].data_disk_name
  zone = element(
    data.google_compute_zones.available.names,
    count.index + var.node_count + 1,
  )
  size     = var.data_disk_size
  type     = lookup(var.per_node_data_disk_type, count.index + var.node_count + 1, var.data_disk_type)
  snapshot = lookup(var.per_node_data_disk_snapshot, count.index + var.node_count + 1, var.data_disk_snapshot)

  labels = merge(var.labels, {
    environment  = var.environment
    pet_name     = var.name
    do_snapshots = "true"
  })

  lifecycle {
    ignore_changes = [snapshot]
  }

  timeouts {
    create = var.data_disk_create_timeout
  }
}

resource "google_compute_disk_resource_policy_attachment" "multizone_data_disk_snapshot_policy_attachment" {
  count = var.hours_between_data_disk_snapshots > 0 ? var.multizone_node_count : 0

  project = var.project
  name    = google_compute_resource_policy.multizone_data_disk_snapshot_policy[count.index].name
  disk    = google_compute_disk.multizone_data_disk[count.index].name
  zone    = google_compute_disk.multizone_data_disk[count.index].zone

  lifecycle {
    # This lifecycle rule is necessary because you cannot modify policies when
    # they are in use
    replace_triggered_by = [
      google_compute_resource_policy.multizone_data_disk_snapshot_policy[count.index].id
    ]
  }
}

# Only take OS disk snapshots of the first instance in a given group.
resource "google_compute_disk_resource_policy_attachment" "mz_instance_with_attached_disk_os_snap_policy_attachment" {
  count = var.enable_os_disk_snapshots && var.multizone_node_count > 0 && var.local_ssd_count == 0 ? 1 : 0

  project = var.project
  name    = google_compute_resource_policy.os_disk_snapshot_policy[0].name
  disk    = google_compute_instance.multizone_instance_with_attached_disk[0].name
  zone    = google_compute_instance.multizone_instance_with_attached_disk[0].zone

  lifecycle {
    replace_triggered_by = [
      google_compute_resource_policy.os_disk_snapshot_policy[0].id
    ]
  }
}

resource "google_compute_disk_resource_policy_attachment" "mz_instance_with_local_ssds_os_snap_policy_attachment" {
  count = var.enable_os_disk_snapshots && var.multizone_node_count > 0 && var.local_ssd_count > 0 ? 1 : 0

  project = var.project
  name    = google_compute_resource_policy.os_disk_snapshot_policy[0].name
  disk    = google_compute_instance.multizone_instance_with_local_ssds[0].name
  zone    = google_compute_instance.multizone_instance_with_local_ssds[0].zone

  lifecycle {
    replace_triggered_by = [
      google_compute_resource_policy.os_disk_snapshot_policy[0].id
    ]
  }
}

resource "google_compute_disk" "multizone_os_disk_from_snapshot" {
  count = var.os_boot_disk_snapshot != "" ? var.multizone_node_count : 0

  snapshot = var.os_boot_disk_snapshot
  name     = local.multi_zone_node_names[count.index].name
  labels   = var.labels
  type     = var.os_disk_type
  zone = element(
    data.google_compute_zones.available.names,
    count.index + var.node_count + 1,
  )
  lifecycle {
    ignore_changes = [snapshot]
  }
}

resource "google_compute_disk" "multizone_log_disk" {
  count = var.multizone_node_count

  project = var.project
  name    = local.multi_zone_node_names[count.index].log_disk_name
  zone = element(
    data.google_compute_zones.available.names,
    count.index + var.node_count + 1,
  )
  size = var.log_disk_size
  type = var.log_disk_type

  labels = merge(var.labels, {
    environment = var.environment
  })
}

resource "google_compute_instance" "multizone_instance_with_attached_disk" {
  count = var.local_ssd_count == 0 ? var.multizone_node_count : 0

  project                   = var.project
  allow_stopping_for_update = var.allow_stopping_for_update
  machine_type              = var.machine_type
  metadata = merge(
    local.instance_metadata,
    {
      "CHEF_NODE_NAME" = local.multi_zone_node_names[count.index].fqdn
    },
  )
  name = local.multi_zone_node_names[count.index].name
  zone = element(
    data.google_compute_zones.available.names,
    count.index + var.node_count + 1,
  )

  service_account {
    // this should be the instance under which the instance should be running, rather than the one creating it...
    email  = var.service_account_email
    scopes = local.service_account_scopes
  }

  scheduling {
    preemptible = var.preemptible
  }

  boot_disk {
    auto_delete = true
    source      = var.os_boot_disk_snapshot == "" ? null : google_compute_disk.multizone_os_disk_from_snapshot[count.index].self_link
    dynamic "initialize_params" {
      for_each = var.os_boot_disk_snapshot == "" ? [0] : []

      content {
        image  = var.os_boot_image
        size   = var.os_disk_size
        type   = var.os_disk_type
        labels = var.labels
      }
    }
  }

  attached_disk {
    source      = google_compute_disk.multizone_data_disk[count.index].self_link
    device_name = var.data_disk_name
  }

  attached_disk {
    source      = google_compute_disk.multizone_log_disk[count.index].self_link
    device_name = "log"
  }

  network_interface {
    subnetwork = local.subnetwork_self_link
    network_ip = var.static_private_ip ? google_compute_address.multizone-static-ip-address[count.index].address : null

    dynamic "access_config" {
      for_each = var.assign_public_ip ? [0] : []
      content {
        nat_ip = var.use_external_ip ? element(local.external_ips, count.index + var.node_count) : ""
      }
    }
  }

  labels = merge(var.labels, {
    environment = var.environment
    pet_name    = var.name
  })

  tags = [
    var.name,
    var.environment,
  ]

  lifecycle {
    ignore_changes = [min_cpu_platform, boot_disk, machine_type]
  }
}

resource "google_compute_instance" "multizone_instance_with_local_ssds" {
  count = var.local_ssd_count > 0 ? var.multizone_node_count : 0

  project                   = var.project
  allow_stopping_for_update = var.allow_stopping_for_update
  machine_type              = var.machine_type
  metadata = merge(
    local.instance_metadata,
    {
      "CHEF_NODE_NAME" = local.multi_zone_node_names[count.index].fqdn
    },
  )
  name = local.multi_zone_node_names[count.index].name
  zone = element(
    data.google_compute_zones.available.names,
    count.index + var.node_count + 1,
  )

  service_account {
    // this should be the instance under which the instance should be running, rather than the one creating it...
    email  = var.service_account_email
    scopes = local.service_account_scopes
  }

  scheduling {
    preemptible = var.preemptible
  }

  boot_disk {
    auto_delete = true
    source      = var.os_boot_disk_snapshot == "" ? null : google_compute_disk.multizone_os_disk_from_snapshot[count.index].self_link
    dynamic "initialize_params" {
      for_each = var.os_boot_disk_snapshot == "" ? [0] : []

      content {
        image  = var.os_boot_image
        size   = var.os_disk_size
        type   = var.os_disk_type
        labels = var.labels
      }
    }
  }

  attached_disk {
    source      = google_compute_disk.multizone_data_disk[count.index].self_link
    device_name = var.data_disk_name
  }

  attached_disk {
    source      = google_compute_disk.multizone_log_disk[count.index].self_link
    device_name = "log"
  }

  scratch_disk {
    interface = "NVME"
  }

  scratch_disk {
    interface = "NVME"
  }

  network_interface {
    subnetwork = local.subnetwork_self_link
    network_ip = var.static_private_ip ? google_compute_address.multizone-static-ip-address[count.index].address : null

    dynamic "access_config" {
      for_each = var.assign_public_ip ? [0] : []
      content {
        nat_ip = var.use_external_ip ? element(local.external_ips, count.index + var.node_count) : ""
      }
    }
  }

  labels = merge(var.labels, {
    environment = var.environment
    pet_name    = var.name
  })

  tags = [
    var.name,
    var.environment,
  ]
}
