locals {
  # This works around terraform limitation of using resources as arrays that
  # don't necessary exist. https://github.com/hashicorp/terraform/issues/9858#issuecomment-386431631
  external_ips = concat(google_compute_address.external[*].address, [""])

  instance_metadata = {
    "CHEF_URL"                     = var.chef_provision["server_url"]
    "CHEF_VERSION"                 = var.chef_provision["version"]
    "CHEF_ENVIRONMENT"             = var.environment
    "GL_KERNEL_VERSION"            = var.kernel_version
    "CHEF_RUN_LIST"                = var.chef_run_list
    "CHEF_DNS_ZONE_NAME"           = var.dns_zone_name
    "CHEF_PROJECT"                 = lookup(var.chef_provision, "bootstrap_gcp_project", var.project)
    "CHEF_BOOTSTRAP_BUCKET"        = var.chef_provision["bootstrap_bucket"]
    "CHEF_BOOTSTRAP_KEYRING"       = var.chef_provision["bootstrap_keyring"]
    "CHEF_BOOTSTRAP_KEY"           = var.chef_provision["bootstrap_key"]
    "CHEF_INIT_RUN_LIST"           = var.chef_init_run_list
    "GL_BOOTSTRAP_DATA_DISK"       = var.bootstrap_data_disk
    "GL_PERSISTENT_DISK_PATH"      = var.persistent_disk_path
    "GL_FORMAT_DATA_DISK"          = var.format_data_disk
    "GL_INITIAL_BOOT_FORCE_REBOOT" = var.initial_boot_force_reboot
    "block-project-ssh-keys"       = upper(tostring(var.block_project_ssh_keys))
    "enable-oslogin"               = upper(tostring(var.enable_oslogin))
    "shutdown-script"              = var.teardown_script != null ? var.teardown_script : module.bootstrap.teardown
    "startup-script"               = var.bootstrap_script != null ? var.bootstrap_script : module.bootstrap.bootstrap
  }

  // all the defaults plus cloudkms to access kms
  service_account_scopes = [
    "https://www.googleapis.com/auth/cloud.useraccounts.readonly",
    "https://www.googleapis.com/auth/devstorage.read_only",
    "https://www.googleapis.com/auth/logging.write",
    "https://www.googleapis.com/auth/monitoring.write",
    "https://www.googleapis.com/auth/pubsub",
    "https://www.googleapis.com/auth/service.management.readonly",
    "https://www.googleapis.com/auth/servicecontrol",
    "https://www.googleapis.com/auth/trace.append",
    "https://www.googleapis.com/auth/cloudkms",
    "https://www.googleapis.com/auth/compute.readonly",
  ]

  multi_zone_node_names = [
    for count in range(var.multizone_node_count) : {
      static_ip_internal_name = format("%v-%02d-%v-%v-static-ip", var.name, count + var.node_count + 1 + 100, var.tier, var.environment)
      static_ip_external_name = format("%v-%02d-%v-%v-static-ip", var.name, count + var.node_count + 1, var.tier, var.environment)
      data_disk_name          = format("%v-%02d-%v-%v-data", var.name, count + var.node_count + 1, var.tier, var.environment)
      log_disk_name           = format("%v-%02d-%v-%v-log", var.name, count + var.node_count + 1, var.tier, var.environment)
      name                    = format("%v-%02d-%v-%v", var.name, var.node_count + count + 1, var.tier, var.environment)
      fqdn                    = format("%v-%02d-%v-%v.c.%v.internal", var.name, count + var.node_count + 1, var.tier, var.environment, var.project)
    }
  ]
}
